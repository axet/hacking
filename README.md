# hacking

## get kernel32 base address x86_64

```
BITS 64

default rel

mov rax, [gs:60h]
mov rax, [rax + 18h]
mov rax, [rax + 20h]
mov rax, [rax]
mov rax, [rax]
mov rax, [rax -10h + 30h]
```

## get kerne32 base address x86

```
BITS 32

default rel

mov eax, [fs:30h]           ; Pointer to PEB (https://en.wikipedia.org/wiki/Win32_Thread_Information_Block)
mov eax, [eax + 0ch]        ; Pointer to Ldr
mov eax, [eax + 14h]        ; Pointer to InMemoryOrderModuleList
mov eax, [eax]              ; this program's module
mov eax, [eax]              ; ntdll module
mov eax, [eax -8h + 18h]    ; kernel32.DllBase (-8h == link next element, 18h - DllBase offset)
```

* https://www.unknowncheats.me/forum/c-and-c-/187123-walking-modules-process-via-peb.html
* https://wasm.in/threads/peb-peb_ldr_data-ldr_module.22693/
* https://cocomelonc.github.io/tutorial/2022/04/02/malware-injection-18.html
* https://wasm.in/threads/peb-peb_ldr_data-ldr_module.22693/
* https://gist.github.com/williballenthin/08891865082a8bd5bf921b58fa312ada
* https://sites.google.com/site/x64lab/home/notes-on-x64-windows-gui-programming/exploring-peb-process-environment-block
